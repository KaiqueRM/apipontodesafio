# Geral
Esta é uma aplicação de marcação de ponto, onde é possível cadastrar e alterar usuários, registrar e consultar suas batidas de ponto.

## Usuário

### GET /users
Exibe a lista de usuários cadastrados

**Response 200**
```json
[
    {
        "id": 1,
        "cpf": "45576913825",
        "email": "kaique.rocha@gmail.com",
        "nome": "Kaique Rocha Morais",
        "data_cadastro": "2019-02-15T18:06:06.192+0000"
    },
    {
        "id": 2,
        "cpf": "16116153792",
        "email": "jose.silva@gmail.com",
        "nome": "José Silva",
        "data_cadastro": "2019-02-16T18:06:06.192+0000"
    }
]
```

**Response 204**
Caso não haja nenhum usuário cadastrado

### GET /user/{id}
Exibe o usuário solicitado pelo parametro id

**Response 200**
```json

{
    "id": 1,
    "cpf": "45576913825",
    "email": "kaique.rocha@gmail.com",
    "nome": "Kaique Rocha Morais",
    "data_cadastro": "2019-02-15T18:06:06.192+0000"
}
```

### POST /user
Cadastra um novo usuário

**Request Body**
```json
{
    "cpf": "45576913825",
    "email": "kaique.rocha@gmail.com",
    "nome": "Kaique Rocha",
    "data_cadastro": "2019-02-15T18:06:06.192+0000"
}
```

**Response 200**

```json
{
    "id": 2,
    "cpf": "45576913825",
    "email": "kaique.rocha@gmail.com",
    "nome": "Kaique Rocha",
    "data_cadastro": "2019-02-15T18:06:06.192+0000"
}
```

**Response 409**
Caso já exista um usuário com o mesmo CPF

```json
{
    "timestamp": "2020-02-20T01:29:39.337+0000",
    "status": 409,
    "error": "Conflict",
    "message": "Já existe um usuário cadastrado com o CPF 4556913825"
}
```

### PUT /user
Atualiza um usuário existente

**Request Body**

```json
{
    "id": 1,
    "cpf": "45576913825",
    "email": "kaique.rocha@gmail.com",
    "nome": "Kaique Rocha Morais"
}
```

**Response 200**

```json
{
    "id": 1,
    "cpf": "45576913825",
    "email": "kaique.rocha@gmail.com",
    "nome": "Kaique Rocha Morais",
    "data_cadastro": "2019-02-15T18:06:06.192+0000"
}
```
**Response 422**
Caso não enconte o usuário a ser atualizado

```json
{
    "timestamp": "2020-02-20T01:33:52.312+0000",
    "status": 422,
    "error": "Unprocessable Entity",
    "message": "Usuário não encontrado para o ID 635"
}
```

## Tipo de Ponto
(Os tipos de ponto são criados automaticamente no startup da aplicação)

### GET /pointTypes

**Response 200**
```json
[
    {
        "id": 1,
        "typeName": "ENTRADA"
    },
    {
        "id": 2,
        "typeName": "SAIDA"
    }
]
```

**Response 204**
Caso não haja nenhum tipo de ponto cadastrado


## Batida de Ponto

### GET /pointHits
Lista todas as batidas de ponto no sistema

**Response 200**
```json
[
    {
        "id": 1,
        "user": {
            "id": 1,
            "cpf": "45576913825",
            "email": "kaique.rocha@gmail.com",
            "nome": "Kaique Rocha Morais",
            "data_cadastro": "2019-02-15T18:06:06.192+0000"
        },
        "hitDateTime": "2020-02-19T03:39:58.662+0000",
        "pointType": {
            "id": 1,
            "typeName": "ENTRADA"
        }
    },
    {
        "id": 2,
        "user": {
            "id": 1,
            "cpf": "45576913825",
            "email": "kaique.rocha@gmail.com",
            "nome": "Kaique Rocha Morais",
            "data_cadastro": "2019-02-15T18:06:06.192+0000"
        },
        "hitDateTime": "2020-02-19T03:40:12.998+0000",
        "pointType": {
            "id": 2,
            "typeName": "SAIDA"
        }
    }
]
```

**Response 204**
Caso não haja nenhuma batida de ponto registrada

### GET /pointHits/{id_user}
Lista todas as batidas de ponto no sistema de um determinado usuário

**Response 200**
```json
[
    {
        "id": 1,
        "user": {
            "id": 1,
            "cpf": "45576913825",
            "email": "kaique.rocha@gmail.com",
            "nome": "Kaique Rocha Morais",
            "data_cadastro": "2019-02-15T18:06:06.192+0000"
        },
        "hitDateTime": "2020-02-19T03:39:58.662+0000",
            "pointType": {
            "id": 1,
            "typeName": "ENTRADA"
        }
    },
    {
        "id": 2,
        "user": {
            "id": 1,
            "cpf": "45576913825",
            "email": "kaique.rocha@gmail.com",
            "nome": "Kaique Rocha Morais",
            "data_cadastro": "2019-02-15T18:06:06.192+0000"
        },
        "hitDateTime": "2020-02-19T03:40:12.998+0000",
        "pointType": {
            "id": 2,
            "typeName": "SAIDA"
        }
    }
]
```

**Response 204**
Caso não haja nenhuma batida de ponto registrada

### POST /pointHit

**Resquest Body**
```json
{
    "id_usuario": 1,
    "id_tipo_registro": 2
}
```

**Respose 200**
```json
{
    "id": 3,
    "user": {
        "id": 1,
        "cpf": "45576913825",
        "email": "kaique.rocha@gmail.com",
        "nome": "Kaique Rocha Morais",
        "data_cadastro": "2019-02-15T18:06:06.192+0000"
    },
    "hitDateTime": "2020-02-20T01:46:29.682+0000",
    "pointType": {
        "id": 2,
        "typeName": "SAIDA"
    }
}
```

**Response 400**
Caso id_usuario ou id_tipo_registro passados resultem em Usuário ou Tipo de Ponto nulos

```json
{
    "timestamp": "2020-02-20T01:50:15.864+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "[Usuário não pode ser nulo, Tipo de registro não pode ser nulo]"
}
```

