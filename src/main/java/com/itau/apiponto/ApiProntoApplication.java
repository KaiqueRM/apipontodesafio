package com.itau.apiponto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiProntoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiProntoApplication.class, args);
	}

}
