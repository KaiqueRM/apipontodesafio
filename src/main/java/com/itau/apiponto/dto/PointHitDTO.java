package com.itau.apiponto.dto;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class PointHitDTO implements Serializable {

    @JsonProperty("id_usuario")
    private int idUser;

    @JsonProperty("data_registro")
    private Date hitDateTime;

    @JsonProperty("id_tipo_registro")
    private int idPointType;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public Date getHitDateTime() {
        return hitDateTime;
    }

    public void setHitDateTime(Date hitDateTime) {
        this.hitDateTime = hitDateTime;
    }

    public int getIdPointType() {
        return idPointType;
    }

    public void setIdPointType(int idPointType) {
        this.idPointType = idPointType;
    }
}
