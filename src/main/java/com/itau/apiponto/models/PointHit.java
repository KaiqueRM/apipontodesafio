package com.itau.apiponto.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PONTO")
public class PointHit implements Serializable {
	
	private static final long serialVersionUID = 178L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private User user;
	
	@Column(name="data_registro")
	private Date hitDateTime;

	@ManyToOne
	@JoinColumn(name = "id_tipo_ponto")
	private PointType pointType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public PointType getPointType() {
		return pointType;
	}

	public void setPointType(PointType pointType) {
		this.pointType = pointType;
	}

	public Date getHitDateTime() { return hitDateTime; }

	public void setHitDateTime(Date hitDateTime) {
		this.hitDateTime = hitDateTime;
	}

}
