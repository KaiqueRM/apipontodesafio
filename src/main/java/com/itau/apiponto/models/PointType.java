package com.itau.apiponto.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="TIPO_PONTO")
public class PointType implements Serializable {

	private static final long serialVersionUID = 12L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="nome_tipo")
	private String typeName;

	@OneToMany(mappedBy = "pointType", cascade=CascadeType.ALL)
	Set<PointHit> pointHitSet;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
