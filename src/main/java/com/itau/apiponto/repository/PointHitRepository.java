package com.itau.apiponto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itau.apiponto.models.PointHit;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PointHitRepository extends JpaRepository<PointHit, Long> {

    @Query("SELECT p FROM PointHit p JOIN p.user u WHERE u.id = ?1")
    List<PointHit> findByUserId(int idUser);

}
