package com.itau.apiponto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itau.apiponto.models.PointType;

public interface PointTypeRepository extends JpaRepository<PointType, Long> {

    PointType findById(int id);

}
