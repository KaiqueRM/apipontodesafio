package com.itau.apiponto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itau.apiponto.models.User;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {
    User findById(int id);

    @Query("SELECT u FROM User u WHERE u.cpf = ?1")
    User findByCpf(String cpf);
}
