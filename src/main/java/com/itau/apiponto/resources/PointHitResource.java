package com.itau.apiponto.resources;

import com.itau.apiponto.dto.PointHitDTO;
import com.itau.apiponto.models.PointHit;
import com.itau.apiponto.services.PointHitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="api")
public class PointHitResource {
    @Autowired
    PointHitService pointHitService;


    @GetMapping("/pointHits")
    public List<PointHit> getPoitHits() {
        return pointHitService.list();
    }

    @GetMapping("/pointHits/{id_user}")
    public List<PointHit> findPointByUser(@PathVariable(value = "id_user") int idUser) {
        return pointHitService.getPointsFromUser(idUser);
    }

    @PostMapping("/pointHit")
    public PointHit savePointHit(@RequestBody PointHitDTO pointDto) {
        return pointHitService.savePointHit(pointDto);
    }
}
