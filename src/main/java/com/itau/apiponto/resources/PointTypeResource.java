package com.itau.apiponto.resources;

import com.itau.apiponto.models.PointType;
import com.itau.apiponto.services.PointTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="api")
public class PointTypeResource {
    @Autowired
    PointTypeService pointTypeService;


    @GetMapping("/pointTypes")
    public List<PointType> getPointTypes() {
        return pointTypeService.list();
    }
}
