package com.itau.apiponto.resources;

import com.itau.apiponto.dto.UserDTO;
import com.itau.apiponto.models.User;
import com.itau.apiponto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="api")
public class UserResource {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public List<User> getUsers(){
        return userService.list();
    }

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable(value = "id") int id){
        return userService.findById(id);
    }

    @PostMapping("/user")
    public User newUser(@RequestBody User user) {
        return userService.create(user);
    }

    @PutMapping("/user")
    public User updateUser(@RequestBody UserDTO user) {
        return userService.update(user);
    }
}
