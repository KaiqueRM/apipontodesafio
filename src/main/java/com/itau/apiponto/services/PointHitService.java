package com.itau.apiponto.services;

import com.itau.apiponto.dto.PointHitDTO;
import com.itau.apiponto.models.PointHit;
import com.itau.apiponto.models.PointType;
import com.itau.apiponto.models.User;
import com.itau.apiponto.repository.PointHitRepository;
import com.itau.apiponto.repository.PointTypeRepository;
import com.itau.apiponto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PointHitService {

    @Autowired
    PointHitRepository pointHitRepository;

    @Autowired
    PointTypeRepository pointTypeRepository;

    @Autowired
    UserRepository userRepository;

    public PointHit savePointHit(PointHitDTO pointDto) {
        PointHit pointHit = new PointHit();
        User user = userRepository.findById(pointDto.getIdUser());
        PointType pointType = pointTypeRepository.findById(pointDto.getIdPointType());
        List<String> errors = new ArrayList<>();
        Boolean isError = false;

        if(user == null) {
            errors.add("Usuário não pode ser nulo");
            isError = true;
        }
        if(pointType == null) {
            errors.add("Tipo de registro não pode ser nulo");
            isError = true;
        }

        if(isError) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errors.toString());
        } else {
            Date date = pointDto.getHitDateTime() == null ? new Date() : pointDto.getHitDateTime();
            pointHit.setHitDateTime(date);
            pointHit.setUser(user);
            pointHit.setPointType(pointType);
            return pointHitRepository.save(pointHit);
        }
    }

    public List<PointHit> getPointsFromUser(int userId) {
        List<PointHit> pointHits = pointHitRepository.findByUserId(userId);
        if(pointHits.isEmpty())
            throw  new ResponseStatusException(HttpStatus.NO_CONTENT);
        return pointHits;

    }

    public List<PointHit> list() {
        List<PointHit> points = pointHitRepository.findAll();
        if(points.isEmpty())
            throw  new ResponseStatusException(HttpStatus.NO_CONTENT);
        return points;

    }
}
