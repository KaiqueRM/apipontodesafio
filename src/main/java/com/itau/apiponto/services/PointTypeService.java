package com.itau.apiponto.services;

import com.itau.apiponto.models.PointType;
import com.itau.apiponto.repository.PointTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class PointTypeService {
    @Autowired
    PointTypeRepository pointTypeRepository;

    public List<PointType> list() {
        List<PointType> pointTypes = pointTypeRepository.findAll();
        if(pointTypes.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        return pointTypeRepository.findAll();
    }


}
