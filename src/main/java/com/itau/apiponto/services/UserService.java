package com.itau.apiponto.services;

import com.itau.apiponto.dto.UserDTO;
import com.itau.apiponto.models.User;
import com.itau.apiponto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User update(UserDTO user) {

        User userEntity = userRepository.findById(user.getId());
        if (userEntity == null) {
            throw  new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Usuário não encontrado para o ID "+user.getId());
        }
        userEntity.setCpf(user.getCpf());
        userEntity.setName(user.getName());
        userEntity.setEmail(user.getEmail());

        return userRepository.save(userEntity);
    }

    public User create (User user) {
        User u = userRepository.findByCpf((user.getCpf()));
        if(u != null)
            throw new ResponseStatusException (HttpStatus.CONFLICT, "Já existe um usuário cadastrado com o CPF " + user.getCpf());
        return userRepository.save(user);
    }

    public User findById(int id) {
        User user = userRepository.findById(id);
        if(user == null)
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Usuário não encontrado");
        return user;

    }

    public List<User> list() {
        List<User> users = userRepository.findAll();
        if(users.isEmpty())
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        return users;
    }

}
