package com.itau.apiponto.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.apiponto.dto.PointHitDTO;
import com.itau.apiponto.models.PointHit;
import com.itau.apiponto.models.PointType;
import com.itau.apiponto.models.User;
import com.itau.apiponto.services.PointHitService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PointHitResource.class)
public class PointHitResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PointHitService pointHitService;

    private ObjectMapper mapper = new ObjectMapper();

    private PointType type;
    private User user;
    private PointHit pointHit;

    @Before
    public void prepare() {
        type = new PointType();
        type.setId(1);
        type.setTypeName("ENTRADA");

        user = new User();
        user.setId(1);
        user.setCpf("45576913825");
        user.setEmail("kaique.rocha@gmail.com");
        user.setName("Kaique Rocha Morais");
        user.setRegistrationDate(new Date());

        pointHit = new PointHit();
        pointHit.setPointType(type);
        pointHit.setUser(user);
        pointHit.setHitDateTime(new Date());
    }

    @Test
    public void shouldCreatePointHit() throws Exception {
        HashMap<String, Object> pointHit = new HashMap<>();
        pointHit.put("id_usuario", "1");
        pointHit.put("id_tipo_registro", "1");

        String pointHitJson = mapper.writeValueAsString(pointHit);

        when(pointHitService.savePointHit(any(PointHitDTO.class))).thenReturn(this.pointHit);

        mockMvc.perform(post("/api/pointHit").contentType(MediaType.APPLICATION_JSON)
                .content(pointHitJson)).andExpect(status().isOk());
    }

    @Test
    public void shouldListPoitHitByUser() throws Exception {
        List<PointHit> pointHits = new ArrayList<>();
        pointHits.add(pointHit);

        when(pointHitService.getPointsFromUser(anyInt())).thenReturn(pointHits);
        mockMvc.perform(get("/api/pointHits/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("id")))
                .andExpect(content().string(containsString("user")))
                .andExpect(content().string(containsString("hitDateTime")))
                .andExpect(content().string(containsString("pointType")));
    }

    @Test
    public void shouldListAllPoitHits() throws Exception {
        List<PointHit> pointHits = new ArrayList<>();
        pointHits.add(pointHit);

        when(pointHitService.list()).thenReturn(pointHits);
        mockMvc.perform(get("/api/pointHits").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("id")))
                .andExpect(content().string(containsString("user")))
                .andExpect(content().string(containsString("hitDateTime")))
                .andExpect(content().string(containsString("pointType")));
    }
}
