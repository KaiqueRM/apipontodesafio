package com.itau.apiponto.resources;

import com.itau.apiponto.models.PointType;
import com.itau.apiponto.services.PointTypeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PointTypeResource.class)
public class PointTypeResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PointTypeService pointTypeService;

    private PointType pointType;

    @Before
    public void prepare() {
        pointType = new PointType();
        pointType.setId(1);
        pointType.setTypeName("ENTRADA");
    }

    @Test
    public void shouldListPointTypes() throws Exception {
        List<PointType> pointTypes = new ArrayList<>();
        pointTypes.add(pointType);

        when(pointTypeService.list()).thenReturn(pointTypes);

        mockMvc.perform(get("/api/pointTypes").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("id")))
                .andExpect(content().string(containsString("typeName")));

    }
}
