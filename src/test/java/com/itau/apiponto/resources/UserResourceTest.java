package com.itau.apiponto.resources;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.apiponto.dto.UserDTO;
import com.itau.apiponto.models.User;
import com.itau.apiponto.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserResource.class)
public class UserResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private User user;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void prepare() {
        user = new User();
        user.setId(1);
        user.setCpf("45576913825");
        user.setEmail("kaique.rocha@gmail.com");
        user.setName("Kaique Rocha Morais");
        user.setRegistrationDate(new Date());
    }

    @Test
    public void shouldCreateUser() throws Exception {
        HashMap<String, Object> user = new HashMap<>();
        user.put("cpf", "76123");
        user.put("email", "76123");
        user.put("data_cadastro", "2019-02-15T18:06:06.192+0000");

        String userJson = mapper.writeValueAsString(user);

        when(userService.create(any(User.class))).then(answer -> answer.getArgument(0));

        mockMvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON)
        .content(userJson)).andExpect(status().isOk());
    }

    @Test
    public void shouldGetUser() throws Exception {
        when(userService.findById(anyInt())).thenReturn(user);
        mockMvc.perform(get("/api/user/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("nome")))
                .andExpect(content().string(containsString("cpf")))
                .andExpect(content().string(containsString("email")))
                .andExpect(content().string(containsString("data_cadastro")))
                .andExpect(content().string(containsString("id")));
    }

    @Test
    public void shouldGetUsers() throws Exception {
        List<User> users = new ArrayList<>();
        users.add(user);
        when(userService.list()).thenReturn(users);
        mockMvc.perform(get("/api/users").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("nome")))
                .andExpect(content().string(containsString("cpf")))
                .andExpect(content().string(containsString("email")))
                .andExpect(content().string(containsString("data_cadastro")))
                .andExpect(content().string(containsString("id")));
    }

    @Test
    public void shouldUpdateUser() throws Exception {

        HashMap<String, Object> userDTO = new HashMap<>();
        userDTO.put("id", "1");
        userDTO.put("cpf", "76123");
        userDTO.put("nome", "teste");
        userDTO.put("email", "76123");

        String userDtoJson = mapper.writeValueAsString(userDTO);

        when(userService.update(any(UserDTO.class))).thenReturn(new User());

        mockMvc.perform(put("/api/user").contentType(MediaType.APPLICATION_JSON)
                .content(userDtoJson)).andExpect(status().isOk());

    }

}
