package com.itau.apiponto.services;

import com.itau.apiponto.dto.PointHitDTO;
import com.itau.apiponto.models.PointHit;
import com.itau.apiponto.models.PointType;
import com.itau.apiponto.models.User;
import com.itau.apiponto.repository.PointHitRepository;
import com.itau.apiponto.repository.PointTypeRepository;
import com.itau.apiponto.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PointHitService.class)

public class PointHitServiceTest {
    @Autowired
    private PointHitService subject;

    @MockBean
    private PointHitRepository pointHitRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private PointTypeRepository pointTypeRepository;

    private PointHit pointHit;
    private PointHitDTO pointHitDTO;
    private User user;
    private PointType type;

    @Before
    public void prepare() {
        type = new PointType();
        type.setId(1);
        type.setTypeName("ENTRADA");

        user = new User();
        user.setId(1);
        user.setCpf("45576913825");
        user.setEmail("kaique.rocha@gmail.com");
        user.setName("Kaique Rocha Morais");
        user.setRegistrationDate(new Date());

        pointHit = new PointHit();
        pointHit.setPointType(type);
        pointHit.setUser(user);
        pointHit.setHitDateTime(new Date());

        pointHitDTO = new PointHitDTO();
        pointHitDTO.setHitDateTime(new Date());
        pointHitDTO.setIdPointType(1);
        pointHitDTO.setIdUser(1);
    }

    @Test(expected = ResponseStatusException.class)
    public void shouldReturnExceptionWhenTypeIsNull() {
        when(userRepository.findById(anyInt())).thenReturn(user);
        when(pointTypeRepository.findById(anyInt())).thenReturn(null);

        subject.savePointHit(pointHitDTO);
    }

    @Test(expected = ResponseStatusException.class)
    public void shouldReturnExceptionWhenUserIsNull() {
        when(userRepository.findById(anyInt())).thenReturn(null);
        when(pointTypeRepository.findById(anyInt())).thenReturn(type);

        subject.savePointHit(pointHitDTO);
    }

    @Test
    public void shouldCreatePointHitWhenUserAndTypeIsNotNull(){
        when(userRepository.findById(anyInt())).thenReturn(user);
        when(pointTypeRepository.findById(anyInt())).thenReturn(type);
        when(pointHitRepository.save(any(PointHit.class))).then(argument -> argument.getArgument(0));

        PointHit response = subject.savePointHit(pointHitDTO);

        assertEquals(response.getId(), pointHit.getId());
    }

    @Test(expected = ResponseStatusException.class)
    public void shouldReturnExceptionWhenNotFoundPointHitByUserId(){
        when(pointHitRepository.findByUserId(anyInt())).thenReturn(new ArrayList<>());
        subject.getPointsFromUser(anyInt());
    }

    @Test
    public void shoudReturnPoitHitListFromUser(){
        List<PointHit> pointHits = new ArrayList<>();
        pointHits.add(pointHit);

        when(pointHitRepository.findByUserId(anyInt())).thenReturn(pointHits);
        List<PointHit> response = subject.getPointsFromUser(anyInt());

        assert(!response.isEmpty());
        assertEquals(response.size(), pointHits.size());
    }

    @Test(expected = ResponseStatusException.class)
    public void shouldReturnExceptionWhenNotFoundPointHit(){
        when(pointHitRepository.findAll()).thenReturn(new ArrayList<>());
        subject.list();
    }

    @Test
    public void shoudReturnPoitHitList(){
        List<PointHit> pointHits = new ArrayList<>();
        pointHits.add(pointHit);

        when(pointHitRepository.findAll()).thenReturn(pointHits);
        List<PointHit> response = subject.list();

        assert(!response.isEmpty());
        assertEquals(response.size(), pointHits.size());
    }


}
