package com.itau.apiponto.services;

import com.itau.apiponto.models.PointType;
import com.itau.apiponto.repository.PointTypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PointTypeService.class)
public class PointTypeServiceTest {
    @Autowired
    private PointTypeService subject;

    @MockBean
    private PointTypeRepository pointTypeRepository;

    private PointType pointType;

    @Before
    public void prepare() {
        pointType = new PointType();
        pointType.setId(1);
        pointType.setTypeName("ENTRADA");
    }

    @Test
    public void shouldListPointTypes() {
        List<PointType> pointTypes = new ArrayList<>();
        pointTypes.add(pointType);
        when(pointTypeRepository.findAll()).thenReturn(pointTypes);

        List<PointType> response = subject.list();

        assertEquals(response.size(), pointTypes.size());
    }

    @Test(expected = ResponseStatusException.class)
    public void shouldReturnExceptionWhenListPointTypesIsEmpty() {
        when(pointTypeRepository.findAll()).thenReturn(new ArrayList<>());
        subject.list();
    }
}
