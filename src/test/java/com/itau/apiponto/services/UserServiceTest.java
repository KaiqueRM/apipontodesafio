package com.itau.apiponto.services;

import com.itau.apiponto.dto.UserDTO;
import com.itau.apiponto.models.User;
import com.itau.apiponto.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UserService.class)
public class UserServiceTest {
    @Autowired
    private UserService subject;

    @MockBean
    private UserRepository userRepository;

    private User user;

    @Before
    public void prepare() {
        user = new User();
        user.setCpf("45576913825");
        user.setEmail("kaique.rocha@gmail.com");
        user.setName("Kaique Rocha Morais");
        user.setRegistrationDate(new Date());
        user.setId(1);
    }

    @Test()
    public void shouldCreateUser() {
        when(userRepository.save(any(User.class))).then(argument -> argument.getArgument(0));
        User response = subject.create(user);
        assertEquals(response, user);
    }

    @Test(expected = ResponseStatusException.class)
    public void shouldReturnExceptionWhenUserIsNull() {
        UserDTO userDTO = new UserDTO();
        when(userRepository.findById(anyInt())).thenReturn(null);
        subject.update(userDTO);
    }

    @Test
    public void shouldUpdateUserexceptRegistrationDate() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1);
        User userToUpdate = new User();
        userToUpdate.setRegistrationDate(user.getRegistrationDate());
        when(userRepository.findById(anyInt())).thenReturn(userToUpdate);
        when(userRepository.save(any(User.class))).then(argument -> argument.getArgument(0));
        User user = subject.update(userDTO);

        assertNotEquals(user.getCpf(), this.user.getCpf());
        assertNotEquals(user.getEmail(), this.user.getEmail());
        assertNotEquals(user.getName(), this.user.getName());
        assertEquals(user.getRegistrationDate(), this.user.getRegistrationDate());
    }

    @Test(expected = ResponseStatusException.class)
    public void shouldReturnExceprionWithUserNull(){
        when(userRepository.findById(anyInt())).thenReturn(null);
        subject.findById(1);
    }

    @Test
    public void shouldReturnUser(){
        when(userRepository.findById(anyInt())).thenReturn(user);
        assertEquals(user, subject.findById(1));
    }

    @Test(expected = ResponseStatusException.class)
    public void shouldReturnExceptionWithListUserNull(){
        when(userRepository.findAll()).thenReturn(new ArrayList<>());
        subject.list();
    }

    @Test
    public void shouldReturnUserList(){
        List<User> users = new ArrayList<>();
        users.add(user);
        when(userRepository.findAll()).thenReturn(users);
        assertEquals(subject.list(), users);
    }
}
